# Variable definitions

```
current = 0 .. 1000 (deci-amps)

camera_shoulder_rotation = 0..90
camera_elbow_rotation = 0..90

gimbal = 0 | 1

link = base | shoulder | elbow | wrist | claw

base = -150..150
shoulder = 0..180
elbow = -150..150
wrist = -90..90
wrist_rot = 0..359
claw_motion = 0 | 1 | 2 (0 is no motion | 1 is claw closing | 2 is claw opening)
claw_torque = 0..?

stuck = 0 .. 31 each of the 5 bits represents:
  base 0x1
  shoulder 0x2
  elbow 0x4
  wrist 0x8
  wrist_rot 0x10
```

# XHR

```
POST /state?camera_shoulder_rotation&camera_elbow_rotation&gimbal&base&shoulder&elbow&wrist&wrist_rot&claw_motion&claw_torque
```

# SSEs

There are four SSEs that the arm MC page supports: hello, ping, stuck, and print.

The hello SSE should be sent with an event name of "hello" whenever the ESP32
detects a new SSE connection. Its data should be a JSON containing the last
values an MC page sent to the arm, and is used to restore the MC page's UI when
its HTML page is loaded or reloaded in a browser. (A pilot might reload the
page if a bug presents itself in the MC page.) The "desired" values in the JSON
should be the last positions a link was requested to move to by an MC page.

The ping SSE should be sent regularly with an event name of "ping". What
regularly means is up to CS's interpretation, but might be every few hundred
milliseconds. Its data should be a JSON with status information about where the
arm's current links are, which, once received, will be presented to the pilot
visually.

The stuck SSE should be sent whenever a link becomes stuck or unstuck and
should have an event name of "stuck". Its data should be a JSON with one field,
"stuck" that is an int whose value is the logical-OR of five bits for base,
shoulder, elbow, wrist, or wrist rotation. (See variable definitions above.)

The print SSE should be sent whenever CS wants to tell the pilot (or anyone
near them) a custom message and should have an event name of "print". Its data
can be an arbitrary string and will be presented verbatim on the MC page as
soon as it's received. The only restriction is that the message must not have any
newline characters.

```
event hello

  camera_shoulder_rotation_desired
  camera_elbow_rotation_desired

  gimbal

  base_desired
  shoulder_desired
  elbow_desired
  wrist_desired
  wrist_rot_desired

  claw_torque

event ping

  current

  camera_shoulder_rotation
  camera_elbow_rotation

  base
  shoulder
  elbow
  wrist
  wrist_rot

event stuck

  stuck

event print

  SSE's data property is just a string with no \r or \n
```

# Mimic -> MC

```
gimbal on

gimbal off

base
<base>

shoulder
<shoulder>

elbow
<elbow>

wristroll
<wrist>

clawposition
<claw>

clawtorque
<claw_torque>
```

# MC -> Mimic

```
stuck:<link>

unstuck:<link>
```
