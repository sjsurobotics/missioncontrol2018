const SP_BOTTOM = 0;
const SP_FIXED = 1;

class CustomConsole {
	constructor() {
		this.element = null;
		this.scrollPosition = SP_BOTTOM;
		this.test_value =  "";
	}

	init() {
		this.element = document.getElementById("log");
		this.element.disabled = true;
		this.element.setAttribute("height","100%");
		this.test_value = this.element.value;
		window.setTimeout(function() {
			if (this.test_value != this.element.value) {
				console.log("hey")
			};
			this.test_value = this.element.value;
		}, 500)
	}
}

//export
let custom_console = new CustomConsole();
export default custom_console;