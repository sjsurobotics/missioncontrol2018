class EventEmitter {
    constructor() {
        this.listeners = {};
    }

    addEventListener(eventType, listener) {
        if (this.listeners[eventType] === undefined) {
            this.listeners[eventType] = [];
        }
        this.listeners[eventType].push(listener);
    }

    emit(eventType, data) {
        if (this.listeners[eventType] === undefined) {
            return;
        }

        for (const listener of this.listeners[eventType]) {
            listener(data);
        }
    }
}
