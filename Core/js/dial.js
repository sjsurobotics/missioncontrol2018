//Please see missioncontrol/Documentation/dials.md for instructions on using the dial.
const svgNS = 'http://www.w3.org/2000/svg';

const defaultStyle = {
    rangeFill: '#e0e0e0',
    remoteArmFill: '#808080',
    localArmFill: '#000000',
    pinFill: '#000000'
};

class Dial {
    constructor(options) {
        const {
            container,
            min,
            max,
            style,
            width,
            height,
            local,
            remote,
            disabled
        } = options;

        this.container = container;
        this.min = min !== undefined ? min : 0;
        this.max = max !== undefined ? max : 360;
        this.style = Object.assign({}, defaultStyle, style);

        if (this.max - this.min > 360) {
            console.error('Dial does not properly handle >360 degree inputs yet');
        }

        this.emitter = new EventEmitter();
        this.width = width || 100;
        this.height = height || 100;

        this.local = local !== undefined ? local : this._defaultValue();
        this.remote = remote !== undefined ? remote : this._defaultValue();
        this.disabled = disabled !== undefined ? disabled : true;

        this.mouseDownListener = e => this.onMouseDown(e);
        this.mouseMoveListener = e => this.onMouseMove(e);
        this.mouseUpListener = e => this.onMouseUp(e);
    }

    _defaultValue() {
        return Math.max(this.min, 0);
    }

    addEventListener(eventType, listener) {
        this.emitter.addEventListener(eventType, listener);
    }

    render() {
        this._assertValid();

        const container = document.querySelector(this.container);

        if (container === null) {
            console.log(`Dial: document.querySelector(${JSON.stringify(this.container)}) returned null`);
            return;
        }

        while (container.firstChild) {
            container.removeChild(container.firstChild);
        }

        container.appendChild(this._renderDial());
    }

    _assertValid() {
        console.assert(Number.isFinite(this.remote));
        console.assert(Number.isFinite(this.local));
        console.assert(this.min <= this.remote && this.remote <= this.max);
        console.assert(this.min <= this.local && this.local <= this.max);
    }

    _renderDial() {
        const { width, height } = this;

        const svg = Dial._createSvgEl('svg', { viewBox: '0 0 2 2', width, height });
        const translation = Dial._createSvgEl('g', {transform: 'translate(1,1)'});

        svg.appendChild(translation);

        translation.appendChild(this._renderRange());
        translation.appendChild(this._renderRemoteArm());
        translation.appendChild(this._renderLocalArm());
        translation.appendChild(this._renderPin());

        svg.addEventListener('mousedown', this.mouseDownListener);

        return svg;
    }

    _renderRange() {
        const { min, max, style } = this;

        const isCircle = max - min >= 360;
        return isCircle ? Dial._createSvgEl('circle', { r: 1, fill: style.rangeFill })
                        : Dial._renderArc(min, max, style.rangeFill);
    }

    _renderRemoteArm() {
        return Dial._renderArm(this.remote, this.style.remoteArmFill);
    }

    _renderLocalArm() {
        return Dial._renderArm(this.local, this.style.localArmFill);
    }

    static _renderArm(value, fill) {
        const angle = Dial._toDegrees(value);
        const transform = `rotate(${-angle} 0 0)`;
        return Dial._createSvgEl('rect', { x: 0, y: -0.1, width: 0.9, height: 0.2, fill, transform });
    }

    _renderPin() {
        return Dial._createSvgEl('circle', { r: 0.25, fill: this.style.pinFill });
    }

    static _renderArc(min, max, fill) {
        const minR = Dial._toRadians(min);
        const maxR = Dial._toRadians(max);

        const x1 =  Math.cos(minR);
        const y1 = -Math.sin(minR);
        const x2 =  Math.cos(maxR);
        const y2 = -Math.sin(maxR);

        const move = 'M0,0';
        const line = `L${x1},${y1}`;
        const arc = `A1,1,0 1,1 ${x2},${y2}`;
        const close = 'Z';

        const d = `${move} ${line} ${arc} ${close}`;

        return Dial._createSvgEl('path', { d, fill });
    }

    static _createSvgEl(tag, attributes) {
        const el = document.createElementNS(svgNS, tag);
        for (const name in attributes) {
            const value = attributes[name];
            el.setAttribute(name, value);
        }
        return el;
    }

    static _toDegrees(value) {
        return -value + 90;
    }

    static _toRadians(value) {
        return Math.PI / 180 * Dial._toDegrees(value);
    }

    onMouseDown(e) {
        e.preventDefault();

        if (this.disabled) {
            return;
        }

        document.documentElement.addEventListener('mousemove', this.mouseMoveListener);
        document.documentElement.addEventListener('mouseup', this.mouseUpListener);

        // Calculate the position of the Dial on the page.
        const svg = e.currentTarget;

        this.svgCenterX = document.documentElement.scrollLeft + svg.getBoundingClientRect().left + this.width / 2;
        this.svgCenterY = document.documentElement.scrollTop + svg.getBoundingClientRect().top + this.height / 2;

        // Calculate where the user grabbed the Dial.
        const mouseX = document.documentElement.scrollLeft + e.clientX;
        const mouseY = document.documentElement.scrollTop + e.clientY;

        this.dragStartAngle = Math.atan2(mouseY - this.svgCenterY, mouseX - this.svgCenterX);
        this.dragStartLocal = this.local;

        this.emitter.emit('down');
    }

    onMouseMove(e) {
        e.preventDefault();

        if (this.disabled) {
            return;
        }

        // Calculate how far the user dragged the Dial from their starting position.
        const mouseX = document.documentElement.scrollLeft + e.clientX;
        const mouseY = document.documentElement.scrollTop + e.clientY;

        const dragAngle = Math.atan(mouseY - this.svgCenterY, mouseX - this.svgCenterX);

        this._setLocal(this.dragStartLocal + (dragAngle - this.dragStartAngle) * 180 / Math.PI);

        this.emitter.emit('move');
    }

    onMouseUp(e) {
        e.preventDefault();

        document.documentElement.removeEventListener('mousemove', this.mouseMoveListener);
        document.documentElement.removeEventListener('mouseup', this.mouseUpListener);

        this.svgCenterX = undefined;
        this.svgCenterY = undefined;
        this.dragStartAngle = undefined;
        this.dragStartLocal = undefined;

        this.emitter.emit('up');
    }

    _setLocal(local) {
        const { min, max } = this;

        while (local > max) {
            local -= 360;
        }
        while (local < min) {
            local += 360;
        }

        if (local < max) {
            // `local` is a valid value.
            this.local = local;
            return;
        }

        // `local` is outside the bounds of `min` or `max`.

        const distanceToMin = 360 - Math.abs(local - min);
        const distanceToMax = Math.abs(max - local);

        if (distanceToMin < distanceToMax) {
            this.local = min;
        } else {
            this.local = max;
        }
    }
}
