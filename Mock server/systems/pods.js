const system = require('../system');

// Pods API
const pod = new system.System('pod');

/* -----------    Variables    ----------- */
let spinOn = 0;
let drillHeight = 0;
let drillProcessProgress = 0;
let temperature;
let humidity;


/* -----------    XHRs    ----------- */
// Spin XHRs
pod.addXHR('/spinOff', (req, res) => {
    console.log('pod/spinOff');
    res.send('OK');

    spinOn = 0;
});

pod.addXHR('/spinOn', (req, res) => {
    console.log('pod/spinOn');
    res.send('OK');

    spinOn = 1;
});


// Drill Process XHRs
//drillProcessProgress -- Equals 0 means process hasn't started. Equals 1 means process started or resumed. Equals 2 means process paused.
pod.addXHR('/startProcess', (req, res) => {
    console.log('pod/processStarted');
    res.send('OK');

	  drillProcessProgress = 1;
});

pod.addXHR('/pauseProcess', (req, res) => {
    console.log('pod/processPaused');
    res.send('OK');

	  drillProcessProgress = 2;
});

pod.addXHR('/resumeProcess', (req, res) => {
    console.log('pod/processResumed');
    res.send('OK');

  	drillProcessProgress = 1;
});

pod.addXHR('/restartProcess', (req, res) => {
    console.log('pod/processRestarted');
    res.send('OK');

	  drillProcessProgress = 0;
});


// Sensor XMRs
pod.addXHR('/getTemperature', (req, res) => {
    console.log('pod/gettingTemperature');
    res.send('OK');

    temperature++;
});

pod.addXHR('/getHumidity', (req, res) => {
    console.log('pod/gettingHumidity');
    res.send('OK');

    humidity++;
});


/* -----------    SSEs    ----------- */
// SSEs
pod.addSSE('ping', 1000, () => {
    return {
        timestamp: Date.now(),
    		spinOn: spinOn,
    		drillPosition: drillHeight,
    		drillProcess: drillProcessProgress,
    		temperature: temperature,
    		humidity: humidity
    };
});
