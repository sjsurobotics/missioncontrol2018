const system = require('../system');

//Drive API
const drive = new system.System('drive');

drive.addSSE('ping', 1000, () => {
    return {
        timestamp: Date.now(),
        current: 50,

        temperature: Math.floor(Math.random() * 100),
        speed: Math.floor(Math.random() * 100),
        rpm: Math.floor(Math.random() * 100),
        trajectory_1: Math.floor(Math.random() * 100),
        trajectory_2: Math.floor(Math.random() * 100),
        trajectory_3: Math.floor(Math.random() * 100),
        trajectory_4: Math.floor(Math.random() * 100),
        heading: Math.floor(Math.random() * 100)
    };
});

drive.addSSE('print', 1000, () => {
   return 'Example print message';
});

drive.addXHR('/', (req, res) => {
    console.log(req.body)
    res.send("OK")
})