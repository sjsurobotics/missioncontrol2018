const Delay = require('../delay');
const system = require('../system');

const SPEED = 10;  // 10 degrees per second or open/close the claw by 10% per second

// State
let power = 0;

const baseDelay = new Delay(-150, 150, SPEED);
const shoulderDelay = new Delay(0, 180, SPEED);
const elbowDelay = new Delay(-150, 150, SPEED);
const wristDelay = new Delay(-130, 130, SPEED);
const wristRotDelay = new Delay(0, 360, SPEED, true);
const clawDelay = new Delay(0, 100, SPEED);
let clawTorque = 100;

let cameraId = 0;
let cameraQuality = 1;
let gimbal = 0;

let stuck = 0;

// Arm API
const arm = new system.System('arm');






arm.addXHR('/power_off', (req, res) => {
    console.log('arm/power_off');
    res.send('OK');

    power = 0;
});

arm.addXHR('/power_on', (req, res) => {
    console.log('arm/power_on');
    res.send('OK');

    power = 1;
});

arm.addXHR('/move', (req, res) => {
    const {
        base,
        shoulder,
        elbow,
        wrist,
        wrist_rot,
        claw
    } = req.body;

    let message = 'arm/move';

    if (base !== undefined) {
        message += ` base=${base}`;
        baseDelay.set(base);
    }
    if (shoulder !== undefined) {
        message += ` shoulder=${shoulder}`;
        shoulderDelay.set(shoulder);
    }
    if (elbow !== undefined) {
        message += ` base=${elbow}`;
        elbowDelay.set(elbow);
    }
    if (wrist !== undefined) {
        message += ` base=${wrist}`;
        wristDelay.set(wrist);
    }
    if (wrist_rot !== undefined) {
        message += ` wrist_rot=${wrist_rot}`;
        wristRotDelay.set(wrist_rot);
    }
    if (claw !== undefined) {
        message += ` claw=${claw}`;
        clawDelay.set(claw);
    }

    console.log(message);
    res.send('OK');
});

arm.addXHR('/torque', (req, res) => {
    const {
        claw_torque,
    } = req.body;

    clawTorque = claw_torque;

    console.log(`arm/torque claw_torque=${claw_torque}`);
    res.send('OK');
});

arm.addXHR('/select_camera', (req, res) => {
    const {
        id,
        quality,
        fps,
        resolution,
        bitrate
    } = req.body;

    if (id === 0) {
        console.log(`arm/select_camera id=${id}`);
    } else {
        console.log(`arm/select_camera id=${id} quality=${quality} fps=${fps} resolution=${resolution} bitrate=${bitrate}`);
    }

    cameraId = id;
    cameraQuality = quality;

    res.send('OK');
});

arm.addXHR('/stop_gimbal', (req, res) => {
    console.log('arm/stop_gimbal');
    res.send('OK');

    gimbal = 0;
});

arm.addXHR('/start_gimbal', (req, res) => {
    console.log('arm/start_gimbal');
    res.send('OK');

    gimbal = 1;
});

arm.addSSE('ping', 1000, () => {
    const getPosition = power ? (delay) => { delay.update(1000); return delay.position; }
                              : (delay) => { return delay.position; };

    return {
        timestamp: Date.now(),
        power,
        current: 50 + Math.floor(Math.random() * 5),

        base: getPosition(baseDelay),
        shoulder: getPosition(shoulderDelay),
        elbow: getPosition(elbowDelay),
        wrist: getPosition(wristDelay),
        wrist_rot: getPosition(wristRotDelay),
        claw: getPosition(clawDelay),
        claw_torque: clawTorque,

        camera_id: cameraId,
        camera_quality: cameraQuality,
        gimbal,

        stuck
    };
});

arm.addSSE('print', 1000, () => {
   return 'Example print message';
});
