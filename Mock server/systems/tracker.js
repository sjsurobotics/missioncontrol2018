const Delay = require('../delay');
const system = require('../system');

// State
let camera = 1;
let stop = 0;
let zoomval = 1;
let focus = 0;
let movespeed = 1;
let moveyaw = 0;
let movepitch = 0;


// Tracker API
const tracker = new system.System('tracker');

tracker.addXHR('/camera1', (req, res) => {
    console.log('tracker/camera1');
    res.send('OK');

    camera = 1;
});

tracker.addXHR('/camera2', (req, res) => {
    console.log('tracker/camera2');
    res.send('OK');

    camera = 2;
});

tracker.addXHR('/zoom', (req, res) => {
    const {
        zoom
    } = req.body;

    console.log(`tracker/zoom=${zoom}`);
    res.send('OK');

    zoomval = zoom;

});

tracker.addXHR('/focus', (req, res) => {
    console.log('tracker/focus');
    res.send('OK');

    focus = 1;
});

tracker.addXHR('/stop', (req, res) => {
    console.log('tracker/stop');
    res.send('OK');

    stop = 1;
});

tracker.addXHR('/run', (req, res) => {
    console.log('tracker/run');
    res.send('OK');

    stop = 0;
});

tracker.addXHR('/speed', (req, res) => {
    const {
        speed
    } = req.body;

    console.log(`tracker/speed=${speed}`);
    res.send('OK');

    movespeed = speed;

});

tracker.addXHR('/yaw', (req, res) => {
    const {
        yaw
    } = req.body;
    console.log(req.body);
    console.log(`tracker/yaw=${yaw}`);
    res.send('OK');

    moveyaw = yaw;
});

tracker.addXHR('/pitch', (req, res) => {
    const {
        pitch
    } = req.body;

    console.log(`tracker/pitch=${pitch}`);
    res.send('OK');

    movepitch = pitch;
});

tracker.addSSE('measuredyaw', 1000, () => {
   return {
   };
});

tracker.addSSE('measuredpitch', 1000, () => {
   return {};
});

tracker.addSSE('debug', 1000, () => {
   return 'Example print message';
});

