
# README #

### General ###
* This README provides instruction on multiple processes:
    1. Hosting MC in your browser
    2. Starting a new subsystem and serving it to your local browser
    3. Sending XHRs (XMLHttpRequest) to + recieving SSEs (Server-sent events) from an ESP32. 
    4. Using the Mock Server

### Repo Hierarchy ###
* APIs_Frameworks - APIs and Frameworks required by subsystem pages.
* Core - Functions and resources shared by all subsystem pages.
* Documentation - Design requirements, guides
  * Currently all required documentation will be stored in this readme
* Mock Server - Server that replicates interacting with an ESP32. 
  * This is used to test XHRs + SSEs without using a physical ESP32.
   * If you want to use a real ESP32, refer the controlsystems2018 README for instructions.
* Subsystems - Pages for each subsystem

### Delpoying MC ###
1. Install NodeJS ```https://nodejs.org/en/```
2. Install required dependencies using NPM.
    * Enter the following in terminal:
        ```
        npm install
        ```
3. Serve /missioncontrol2018 to your browser.
    * Enter the following in terminal:
        ```
        node .
        ```
    * Navigate to localhost:8000 to see the folder.

### Mock Server ###
The mock server is a testing tool used to spoof connection to a physical ESP32. The mock server can receive XHRs and send SSEs to mission control pages. To use it, refer to in instructions located in *Mock server/README.md*. **You must create a system before sending XHRs and SSEs**.

### XHRs ###
Data can be sent to the server using sendXHR() located in Core/js/sendXHR.js.  

### SSEs ###
SSEs (Server Side Events) are handled using the EventSource class.


1. Instantiate the EventSource object.
    ```
    let eventSource = new EventSource(esp32_address + "/sse");
    ```
    * Find *esp32_address*
        * Start the mock server by following the instructions in ./*Mock server*
        * Once started, the mock server will output all servers' addresses. esp32_address is your server's address.

2. Add event listeners for any SSEs you specified in your Mock Server file. In the following example, the only SSE we're expecting is 'ping'.
    ```
    eventSource.addEventListener('ping', pingRecieved);
    ```
    ```
    function pingRecieved(message) {
        console.log(message.data);
    }
    ```
    