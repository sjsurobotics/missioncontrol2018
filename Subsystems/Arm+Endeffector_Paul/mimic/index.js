const express = require('express');
const SerialPort = require('serialport');

const SERIAL_DEVICE = '/dev/ttyUSB0';
const SERIAL_BAUD_RATE = 115200;

const INPUT_MODE_COMMAND = 0;
const INPUT_MODE_BASE = 1;
const INPUT_MODE_SHOULDER = 2;
const INPUT_MODE_ELBOW = 3;
const INPUT_MODE_WRIST_POSITION = 4;
const INPUT_MODE_WRIST_ROTATION = 5;
const INPUT_MODE_CLAW_TORQUE = 6;

let partialLine = '';
let inputMode = INPUT_MODE_COMMAND;
var WebSocketServer = require('ws').Server;
wss = new WebSocketServer({port: 40510});
let ws = null;

wss.on('connection', function (socket) {
    socket.on('message', function (message) {
      console.log('received: %s', message)
    })
  
    //this assignment allows you to use the socket once the connection has been established.
    ws = socket;
  })

function receiveData(data) {
    partialLine += data.toString('utf-8');
    const split = partialLine.split('\r\n');
    partialLine = split[split.length - 1];
    for (const line of split.slice(0, -1)) {
        processLine(line);
    }
}

function map(number, in_min, in_max, out_min, out_max)
{
    var temp =  (number - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
    // console.log("temp: ", temp);
    return temp;
}

function processLine(line) {
    console.log(`USB line ${line}`, inputMode);
    switch (inputMode) {
        case INPUT_MODE_COMMAND:
            processCommandLine(line);
            break;
        case INPUT_MODE_BASE:
            // Don't need Number.parseInt because we're just serializing it again.
            var b = Number.parseInt(line.slice(5), 10);
            // console.log("b: ", b);
            console.log("base: ", map(b, 0, 270, -150, 150));
            writeMC('base', map(b, 0, 270, -150, 150));
            inputMode = INPUT_MODE_COMMAND;
            break;
        case INPUT_MODE_SHOULDER:
            var b = Number.parseInt(line.slice(9), 10);
            // console.log("b: ", b);
            console.log("shoulder: ", map(b, 0, 180, 0, 180));
            writeMC('shoulder', map(b, 0, 180, 0, 180));
            inputMode = INPUT_MODE_COMMAND;
            break;
        case INPUT_MODE_ELBOW:
            var b = Number.parseInt(line.slice(6), 10);
            // console.log("b: ", b);
            console.log("elbow: ", map(b, 0, 270, -150, 150));
            writeMC('elbow', map(b, 0, 270, -150, 150));
            inputMode = INPUT_MODE_COMMAND;
            break;
        case INPUT_MODE_WRIST_POSITION:
            var b = Number.parseInt(line.slice(15), 10);
            // console.log("b: ", b);
            console.log("wrist pitch: ", map(b, 0, 130, -90, 90));
            writeMC('wrist position', map(b, 0, 130, -90, 90));
            inputMode = INPUT_MODE_COMMAND;
            break;
        case INPUT_MODE_WRIST_ROTATION:
            var b = Number.parseInt(line.slice(15), 10);
            // console.log("b: ", b);
            console.log("wrist rotation: ", map(b, 0, 180, 0, 360));
            writeMC('wrist rotation', map(b, 0, 180, 0, 360));
            inputMode = INPUT_MODE_COMMAND;
            break;
        case INPUT_MODE_CLAW_TORQUE:
            var b = Number.parseInt(line.slice(12), 10);
            // console.log("b: ", b);
            console.log("claw torque: ", map(b, 0, 100, 0, 100));
            writeMC('claw torque', map(b, 0, 100, 0, 100));
            inputMode = INPUT_MODE_COMMAND;
            break;
    }
    // console.log(line);
}

function processCommandLine(line) {
    console.log(line);
    // websocket.send(line);
        // if(line.includes("gimbal on")
        // {

        // }
        // else if(line.includes("gimbal off")
        // {

        // }
        // else if(line.includes("clawopen down")
        // {

        // }
        // else if(line.includes("clawopen up")
        // {

        // }
        // else if(line.includes("clawclose down")
        // {

        // }
        if (line.includes("clawClose") ) {
            writeMC(line);
        } 
        else if (line.includes("clawOpen") ) {
            writeMC(line);
        } 
        else if (line.includes("gimbal on") ) {
            writeMC(line);
        } 
        else if (line.includes("gimbal off") ) {
            writeMC(line);
        } 
        else if(line.includes("base") )
        {
            inputMode = INPUT_MODE_BASE;
            console.log(inputMode);
        }
        else if(line.includes("shoulder") )
        {
            inputMode = INPUT_MODE_SHOULDER;
            console.log(inputMode);
         }
        else if(line.includes("elbow") )
        {
            inputMode = INPUT_MODE_ELBOW;
            console.log(inputMode);
        }
        else if(line.includes("wrist position") )
        {
            inputMode = INPUT_MODE_WRIST_POSITION;
            console.log(inputMode);
        }
        else if(line.includes("wrist rotation") )
        {
            inputMode = INPUT_MODE_WRIST_ROTATION;
            console.log(inputMode);
        }
        else if(line.includes("claw torque") )
        {
            inputMode = INPUT_MODE_CLAW_TORQUE;
            console.log(inputMode);
        }
        else
        {
            inputMode = INPUT_MODE_COMMAND;
        }

    // switch (line) {
        // case "gimbal on":
        // case "gimbal off":
        // case "clawopen down":
        // case "clawopen up":
        // case "clawclose down":
        // case "clawclose up":
        //     writeMC(line);
        //     break;
        // case "base":
        //     inputMode = INPUT_MODE_BASE;
        //     console.log(inputMode);
        //     break;
        // case "shoulder":
        //     inputMode = INPUT_MODE_SHOULDER;
        //     console.log(inputMode);
        //     break;
        // case "elbow":
        //     inputMode = INPUT_MODE_ELBOW;
        //     console.log(inputMode);
        //     break;
        // case "wrist position":
        //     inputMode = INPUT_MODE_WRIST_POSITION;
        //     console.log(inputMode);
        //     break;
        // case "wrist rotation":
        //     inputMode = INPUT_MODE_WRIST_ROTATION;
        //     console.log(inputMode);
        //     break;
        // case "claw torque":
        //     inputMode = INPUT_MODE_CLAW_TORQUE;
        //     console.log(inputMode);
        //     break;
    // }
console.log(inputMode);
}

function handleStuck(req, res) {
    const {link} = JSON.parse(req.body);
    writeMimic(`stuck:${link}`);
}

function handleUnstuck(req, res) {
    const {link} = JSON.parse(req.body);
    writeMimic(`unstuck:${link}`);
}

function startHttpServer() {
    const app = express();
    app.post('/stuck', handleStuck);
    app.post('/unstuck', handleUnstuck);
    app.listen(2000);
}

function connectMimic() {
    port = new SerialPort(SERIAL_DEVICE, { baudRate: SERIAL_BAUD_RATE });
    // for (f in port) {
    //     console.log(f);
    // }
    // port.on('open', () => console.log('USB open'));
    port.on('data', receiveData);
    // port.on('error', e => console.log('USB error', e));
    // port.on('close', () => console.log('USB close'));
}

function writeMimic(msg) {
    if (port !== null) {
        port.write(msg, 'utf-8');
    }
}

function writeMC(msg, value) {
    //this conditional only succeeds 
    // console.log(msg, value);
    if (ws) {
        ws.send(msg+value);
    }
}

function main() {
    startHttpServer();
    connectMimic();

    //Placeholder for acutally sending data since I didn't have mimic at time of making this fix. @chew comment this out.
    // setInterval(() => {
    //     writeMC('shoulder', 1);
    // }, 100);
}

main();
