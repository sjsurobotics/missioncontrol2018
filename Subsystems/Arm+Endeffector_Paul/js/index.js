import {
    connectClicked,
    camNoneClicked,
    camElbowClicked,
    camWristClicked,
    ctrlMouseClicked,
    ctrlPreviewClicked,
    ctrlMimicClicked,
    ctrlGimbalOffClicked,
    ctrlGimbalOnClicked,
    ctrlResetClicked,
    dialCamMoved,
    dialArmMoved,
    ctrlClawCloseDown,
    ctrlClawCloseUp,
    ctrlClawOpenDown,
    ctrlClawOpenUp,
    updatePositionClicked
} from "./handlers.js";

import {
    state
} from "./model.js";

import {
    rerender
} from "./rendering.js";



function addEventListener(selector, eventType, listener) {
    document.querySelector(selector).addEventListener(eventType, listener);
}

function initializeCurrent() {
    state.currentChart = Highcharts.chart('graph', {
        title: {
            text: undefined
        },

        yAxis: {
            title: {
                text: 'mA'
            }
        },

        series: [{
            name: 'mA',
            data: []
        }],
    });
}

function main() {
    // Connection
    addEventListener('#connect', 'click', connectClicked);

    document.getElementById('host').value = sessionStorage.getItem('host');

    // Power
    addEventListener('#cam-id-none', 'click', camNoneClicked);
    addEventListener('#cam-id-elbow', 'click', camElbowClicked);
    addEventListener('#cam-id-wrist', 'click', camWristClicked);

    state.dialCamShoulder = new Dial({ container: '#dial-cam-shoulder', min: 0, max: 90 });
    state.dialCamShoulder.addEventListener('move', dialCamMoved);

    state.dialCamElbow = new Dial({ container: '#dial-cam-elbow', min: 0, max: 90 });
    state.dialCamElbow.addEventListener('move', dialCamMoved);

    // Health
    initializeCurrent();

    // Control
    addEventListener('#ctrl-mouse', 'click', ctrlMouseClicked);
    addEventListener('#ctrl-preview', 'click', ctrlPreviewClicked);
    addEventListener('#ctrl-mimic', 'click', ctrlMimicClicked);

    addEventListener('#ctrl-gimbal-off', 'click', ctrlGimbalOffClicked);
    addEventListener('#ctrl-gimbal-on', 'click', ctrlGimbalOnClicked);

    addEventListener('#ctrl-reset', 'click', ctrlResetClicked);

    // state.dialBase = new Dial({ container: '#dial-base', min: -150, max: 150 });
    // state.dialBase.addEventListener('move', dialArmMoved);

    // state.dialShoulder = new Dial({ container: '#dial-shoulder', min: 0, max: 180 });
    // state.dialShoulder.addEventListener('move', dialArmMoved);

    // state.dialElbow = new Dial({ container: '#dial-elbow', min: -150, max: 150 });
    // state.dialElbow.addEventListener('move', dialArmMoved);

    // state.dialWrist = new Dial({ container: '#dial-wrist', min: -130, max: 130 });
    // state.dialWrist.addEventListener('move', dialArmMoved);

    // state.dialWristRot = new Dial({ container: '#dial-wrist-rot', min: 0, max: 360 });
    // state.dialWristRot.addEventListener('move', dialArmMoved);

    addEventListener('#ctrl-claw-close', 'mousedown', ctrlClawCloseDown);
    addEventListener('#ctrl-claw-close', 'mouseup',   ctrlClawCloseUp);
    addEventListener('#ctrl-claw-open',  'mousedown', ctrlClawOpenDown);
    addEventListener('#ctrl-claw-open',  'mouseup',   ctrlClawOpenUp);

    addEventListener('#update-position', 'click', updatePositionClicked);

    rerender();
}

document.addEventListener('DOMContentLoaded', main);
