import {
    sendState,
    sendStuck,
} from "./dispatcher.js";

import {
    CS_DISCONNECTED,
    CS_CONNECTING,
    CS_CONNECTED,

    CI_NONE,
    CI_ELBOW,
    CI_WRIST,

    CM_MOUSE,
    CM_PREVIEW,
    CM_MIMIC,

    GS_OFF,
    GS_ON,

    CLAW_STATIONARY,
    CLAW_CLOSE,
    CLAW_OPEN,

    state,

    appendCurrentHistory,
    appendPrintMessage
} from "./model.js";

import {
    rerender
} from "./rendering.js";

var ws = new WebSocket('ws://localhost:40510');

ws.onopen = function() {
    console.log('websocket is connected ...');
    ws.send('connected');
}

ws.onmessage = function(ev) {
    console.log(ev);
    if (ev.data.toString().includes("clawclose up") ) {
        writeMC(ev.data.toString());
    } 
    else if(ev.data.toString().includes("base") )
    {
        state.dialBase = Number.parseInt(ev.data.toString().slice(4), 10);
    }
    else if(ev.data.toString().includes("shoulder") )
    {
        state.dialShoulder = Number.parseInt(ev.data.toString().slice(8), 10);
     }
    else if(ev.data.toString().includes("elbow") )
    {
        state.dialElbow = Number.parseInt(ev.data.toString().slice(5), 10);
    }
    else if(ev.data.toString().includes("wrist position") )
    {
        state.dialWrist = Number.parseInt(ev.data.toString().slice(14), 10);
    }
    else if(ev.data.toString().includes("wrist rotation") )
    {
        state.dialWristRot = Number.parseInt(ev.data.toString().slice(14), 10);
    }
    else if(ev.data.toString().includes("claw torque") )
    {
        console.log(inputMode);
    }
    else if(ev.data.toString().includes("clawClose") )
    {
        state.clawMotion = 1;
    }
    else if(ev.data.toString().includes("clawOpen") )
    {
        state.clawMotion = 2;
    }
    else if(ev.data.toString().includes("gimbal on") )
    {
        state.gimbalState = 1;
    }
    else if(ev.data.toString().includes("gimbal off") )
    {
        state.gimbalState = 0;
    }
    else
    {
        state.clawMotion = 0;
        inputMode = INPUT_MODE_COMMAND;
    }
    state.clawMotion = claw_motion;
    //state.sliderClawTorque = claw_torque;

    if (state.ctrlMode === CM_MIMIC) {
        sendState();
    }

    rerender();
}

export function connectionOpened() {
    state.robotConnectionState = CS_CONNECTED;
    state.robotConnectionErrored = false;
    rerender();
}

export function connectionErrored() {
    state.robotConnectionState = CS_DISCONNECTED;
    state.robotConnectionErrored = true;
    rerender();
}

export function hello(msg) {
    const obj = parseObject(msg.data);

    const {
        camera_id,

        camera_shoulder_rotation_desired,
        camera_elbow_rotation_desired,

        gimbal,

        base_desired,
        shoulder_desired,
        elbow_desired,
        wrist_desired,
        wrist_rot_desired,
        claw_desired,

        claw_torque,
    } = obj;

    state.camId = camera_id;

    // FIXME: Revisit this when mimic camera control buttons work.
    state.dialCamShoulder = camera_shoulder_rotation_desired;
    state.dialCamElbow = camera_elbow_rotation_desired;

    state.gimbalState = gimbal;

    if (state.ctrlMode === CM_MOUSE) {
        state.dialBase = base_desired;
        state.dialShoulder = shoulder_desired;
        state.dialElbow = elbow_desired;
        state.dialWrist = wrist_desired;
        state.dialWristRot = wrist_rot_desired;
    }

    rerender();

    sendState();  // Send claw_motion (which *should* be CLAW_STATIONARY) to
                  // stop the claw if it's moving.
}

export function ping(msg) {
    const obj = parseObject(msg.data);

    const {
        current,

        camera_shoulder_rotation,
        camera_elbow_rotation,

        base,
        shoulder,
        elbow,
        wrist,
        wrist_rot,
    } = obj;

    appendCurrentHistory(current);

    state.dialCamShoulder.remote = camera_shoulder_rotation;
    state.dialCamElbow.remote = camera_elbow_rotation;

    state.dialBase.remote = base;
    state.dialShoulder.remote = shoulder;
    state.dialElbow.remote = elbow;
    state.dialWrist.remote = wrist;
    state.dialWristRot.remote = wrist_rot;

    rerender();
}

export function stuck(msg) {
    const stuck = Number.parseInt(msg.data);

    sendStuck(stuck);
}

export function print(msg) {
    appendPrintMessage(msg.data);
    rerender();
}

export function mimic(msg) {
    if (state.ctrlMode === CM_MOUSE) {
        return;
    }
    console.log(msg);
    const {
        gimbal,

        base,
        shoulder,
        elbow,
        wrist,
        wrist_rot,
        claw_motion,
        claw_torque,
    } = JSON.parse(msg.data);

    state.gimbalState = gimbal;

    state.dialBase = base;
    state.dialShoulder = shoulder;
    state.dialElbow = elbow;
    state.dialWrist = wrist;
    state.dialWristRot = wrist_rot;

    state.clawMotion = claw_motion;
    //state.sliderClawTorque = claw_torque;

    if (state.ctrlMode === CM_MIMIC) {
        sendState();
    }

    rerender();
}


//
// UI event handlers
//

export function connect() {
    disconnect();

    sessionStorage.setItem('host', document.getElementById('host').value);

    state.host = 'http://' + document.getElementById('host').value;

    state.eventSource = new EventSource(state.host + '/sse');

    state.eventSource.addEventListener('open', connectionOpened);
    state.eventSource.addEventListener('error', connectionErrored);

    state.eventSource.addEventListener('hello', hello);
    state.eventSource.addEventListener('ping', ping);
    state.eventSource.addEventListener('stuck', stuck);
    state.eventSource.addEventListener('print', print);

    state.robotConnectionState = CS_CONNECTING;

    rerender();
}

export function disconnect() {
    if (state.eventSource !== null) {
        state.eventSource.close();
        state.eventSource = null;
    }

    state.robotConnectionState = CS_DISCONNECTED;
    rerender();
}

export function connectClicked() {
    switch (state.robotConnectionState) {
        case CS_DISCONNECTED:
            connect();
            break;
        case CS_CONNECTING:
            disconnect();
            break;
        case CS_CONNECTED:
            disconnect();
            break;
    }
}

export function camNoneClicked() {
    state.camId = CI_NONE;
    rerender();

    sendState();
}

export function camElbowClicked() {
    state.camId = CI_ELBOW;
    rerender();

    sendState();
}

export function camWristClicked() {
    state.camId = CI_WRIST;
    rerender();

    sendState();
}

export function ctrlMouseClicked() {
    state.ctrlMode = CM_MOUSE;
    rerender();
}

export function ctrlPreviewClicked() {
    state.ctrlMode = CM_PREVIEW;
    rerender();
}

export function ctrlMimicClicked() {
    state.ctrlMode = CM_MIMIC;
    rerender();
}

export function ctrlGimbalOffClicked() {
    state.gimbalState = GS_OFF;
    rerender();

    sendState();
}

export function ctrlGimbalOnClicked() {
    state.gimbalState = GS_ON;
    rerender();

    sendState();
}

export function ctrlResetClicked() {
    state.dialBase = 0;
    state.dialShoulder = 0;
    state.dialElbow = 0;
    state.dialWrist = 0;
    state.dialWristRot = 0;

    state.dialCamShoulder = 0;
    state.dialCamElbow = 0;

    rerender();

    sendState();
}

export function updatePositionClicked() {
    console.log(state)
    state.dialBase = document.querySelector("#rotunda").value;
    state.dialShoulder = document.querySelector("#shoulder").value;
    state.dialElbow = document.querySelector("#elbow").value;
    state.dialWrist = document.querySelector("#wrist-pitch").value;
    state.dialWristRot = document.querySelector("#wrist-rot").value;

    sendState();
}

export function dialCamMoved() {
    rerender();

    sendState();
}

export function dialArmMoved() {
    rerender();

    sendState();
}

export function ctrlClawCloseDown() {
    state.clawMotion = CLAW_CLOSE;

    rerender();

    sendState();
}

export function ctrlClawCloseUp() {
    state.clawMotion = CLAW_STATIONARY;

    rerender();

    sendState();
}

export function ctrlClawOpenDown() {
    state.clawMotion = CLAW_OPEN;

    rerender();

    sendState();
}

export function ctrlClawOpenUp() {
    state.clawMotion = CLAW_STATIONARY;

    rerender();

    sendState();
}
