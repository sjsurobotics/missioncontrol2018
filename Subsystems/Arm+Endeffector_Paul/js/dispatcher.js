import {
    state,

    appendPrintMessage
} from "./model.js";



//
// Arm XHRs
//

function xhrError() {
    appendPrintMessage('Mission Control: Failed sending command to Control Systems');
}

function sendXHR(path, data) {
   const xhr = new XMLHttpRequest();
   xhr.addEventListener('error', xhrError);
   const body = data ? formatObject(data) : undefined;
//    xhr.onreadystatechange = function() {
//         if (xhr.readyState == XMLHttpRequest.DONE) {
//             alert(xhr.responseText);
//         }
//     }
   console.log(state.host+'/?data=' + body);
   xhr.open('POST', state.host +'/?data='+ body);
   xhr.send();
}

export function sendState() {
    console.log(state);

    sendXHR('/state', {
        camera_id: state.camId,
        camera_shoulder_rotation: Math.round(state.dialCamShoulder),
        camera_elbow_rotation: Math.round(state.dialCamElbow),
        gimbal: state.gimbalState,
        base: Math.round(state.dialBase),
        shoulder: Math.round(state.dialShoulder),
        elbow: Math.round(state.dialElbow),
        wrist: Math.round(state.dialWrist),
        wrist_rot: Math.round(state.dialWristRot),
        claw_motion: state.clawMotion,
        claw_torque: Math.round(0),
    });
}



//
// Mimic commands
//

export function sendStuck(stuck) {
}

function formatObject(obj) {
    let str = '{';
    let first = true;
    for (const key in obj) {
        const value = obj[key];
        if (value === undefined || value === null) {
            continue;
        }
        if (first) {
            first = false;
        } else {
            str += ',';
        }
        str += `${key}:${JSON.stringify(value)}`;
    }
    str += '}';
    return str;
}