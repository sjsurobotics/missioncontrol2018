//
// Constants
//

export const CS_DISCONNECTED = 0;
export const CS_CONNECTING = 1;
export const CS_CONNECTED = 2;

export const CI_NONE = 0;
export const CI_ELBOW = 1;
export const CI_WRIST = 2;

export const MAX_CURRENT_HISTORIES = 10;
export const MAX_PRINT_MESSAGES = 1000;

export const CM_MOUSE = 0;
export const CM_PREVIEW = 1;
export const CM_MIMIC = 2;

export const GS_OFF = 0;
export const GS_ON = 1;

export const CLAW_STATIONARY = 0;
export const CLAW_CLOSE = 1;
export const CLAW_OPEN = 2;



//
// State
//

export const state = {
    rerendering: false,

    // Connection
    host: null,
    robotConnectionState: CS_DISCONNECTED,
    mimicConnectionState: CS_DISCONNECTED,
    eventSource: null,
    robotConnectionErrored: false,

    // Power
    camId: CI_NONE,
    dialCamShoulder: null,
    dialCamElbow: null,

    // Health
    currentHistories: [],
    currentHistoriesLastModified: Date.now(),
    printMessages: [],
    printMessagesLastModified: Date.now(),
    currentChart: null,

    // Control
    ctrlMode: CM_MOUSE,
    gimbalState: GS_OFF,
    dialBase: null,
    dialShoulder: null,
    dialElbow: null,
    dialWrist: null,
    dialWristRot: null,
    clawMotion: CLAW_STATIONARY,
    sliderClawTorque: null,
};


export function appendCurrentHistory(mA) {
    state.currentHistories.push(mA);

    if (state.currentHistories.length > MAX_CURRENT_HISTORIES) {
        state.currentHistories.shift();
    }

    state.currentHistoriesLastModified = Date.now();
}

export function appendPrintMessage(msg) {
    state.printMessages.push(msg);

    if (state.printMessages.length > MAX_PRINT_MESSAGES) {
        state.printMessages.shift();
    }

    state.printMessagesLastModified = Date.now();
}

export function cloneState() {
    const clone = Object.assign({}, state);
    for (const key in clone) {
        if (state[key] instanceof Dial) {
            clone[key] = cloneDial(clone[key]);
        }
    }
    return clone;
}

function cloneDial(dial) {
    return {
        local: dial.local,
        remote: dial.remote,
    };
}

// Create an object listing which properties have changed since a clone of the
// state was taken.
export function diffState(clone) {
    const diff = {};
    for (const key in state) {
        if (state[key] instanceof Dial) {
            diff[key] = diffDial(clone[key], state[key]);
        } else if (clone[key] !== state[key]) {
            diff[key] = true;
        }
    }
    return diff;
}

function diffDial(clone, original) {
    return clone && (clone.local !== original.local || clone.remote !== original.remote);
}
